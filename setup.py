#    ssipfootprint provides automated batch SSIP footprinting functionality.
#    Copyright (C) 2020  Mark D. Driver
#
#    ssipfootprint is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
from setuptools import setup
import setuptools

setup(name='ssipfootprint',
      use_scm_version=True,
      setup_requires=['setuptools_scm'],
      description='CLI for Automated batch processing of SSIP footprinting operations',
      url='https://bitbucket.org/mdd31/ssipfootprint',
      author='Mark Driver',
      author_email='mdd31@cam.ac.uk',
      license='AGPLv3',
      packages=setuptools.find_packages(),
      package_data={'':["test/resources/*.*","test/resources/*/*","test/resources/*/*/*"]},
      zip_safe=False)
