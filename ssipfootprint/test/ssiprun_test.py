#    ssipfootprint provides automated batch SSIP footprinting functionality.
#    Copyright (C) 2020  Mark D. Driver
#
#    ssipfootprint is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
# -*- coding: utf-8 -*-
"""
Script containing test case for ssip_run methods.

:Authors:
    Mark Driver <mdd31>
"""

import logging
import unittest
import sys
import ssipfootprint.ssip_run as ssiprun

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)

class SSIPRunTestCase(unittest.TestCase):
    ""
    def setUp(self):
        """Set up before test.

        Returns
        -------
        None.

        """
        self.inchikey = "XLYOFNOQVPJJNP-UHFFFAOYSA-N"
        self.cmlfilename = self.inchikey +".cml"
    def tearDown(self):
        """Clean up after tests.

        Returns
        -------
        None.

        """
        del self.inchikey
        del self.cmlfilename
    def test_create_ssip_call_argument_list_tri_surf(self):
        """Test expected call is created.

        Returns
        -------
        None.

        """
        expected_list = ['ssip',
                         '-t',
                         '-m',
                         'XLYOFNOQVPJJNP-UHFFFAOYSA-N_0.0020_merged.cube',
                         '-g',
                         'XLYOFNOQVPJJNP-UHFFFAOYSA-N_0.0050_merged.cube',
                         '-f',
                         'XLYOFNOQVPJJNP-UHFFFAOYSA-N_0.0104_merged.cube',
                         '-c',
                         'XLYOFNOQVPJJNP-UHFFFAOYSA-N.cml',
                         '-w',
                         'XLYOFNOQVPJJNP-UHFFFAOYSA-N_ssip.xml']
        actual_list = ssiprun.create_ssip_call_argument_list_tri_surf(self.cmlfilename)
        self.assertListEqual(expected_list, actual_list)
    def test_create_ssip_call_argument_list_mono_surf(self):
        """Test expected call is created.

        Returns
        -------
        None.

        """
        expected_list = ['ssip',
                         '-m',
                         'XLYOFNOQVPJJNP-UHFFFAOYSA-N_0.0020_merged.cube',
                         '-c',
                         'XLYOFNOQVPJJNP-UHFFFAOYSA-N.cml',
                         '-w',
                         'XLYOFNOQVPJJNP-UHFFFAOYSA-N_ssip.xml']
        actual_list = ssiprun.create_ssip_call_argument_list_mono_surf(self.cmlfilename)
        self.assertListEqual(expected_list, actual_list)
    def test_create_ssip_call(self):
        """Test expected call list created.

        Returns
        -------
        None.

        """
        expected_list = ["ssip"]
        actual_list = ssiprun.create_ssip_call()
        self.assertListEqual(expected_list, actual_list)
        expected_jar_list = ["java", "-jar", "example.jar"]
        actual_jar_list = ssiprun.create_ssip_call(ssip_call="example.jar")
    def test_create_ssip_filename(self):
        """Test expected filename produced.

        Returns
        -------
        None.

        """
        expected_filename = "XLYOFNOQVPJJNP-UHFFFAOYSA-N_ssip.xml"
        actual_filename = ssiprun.create_ssip_filename(self.inchikey)
        self.assertEqual(expected_filename, actual_filename)
    def test_create_cube_filenames_tri_surf(self):
        """Test expected filenames are returned.

        Returns
        -------
        None.

        """
        expected_filenames = ('XLYOFNOQVPJJNP-UHFFFAOYSA-N_0.0020_merged.cube',
                              'XLYOFNOQVPJJNP-UHFFFAOYSA-N_0.0050_merged.cube',
                              'XLYOFNOQVPJJNP-UHFFFAOYSA-N_0.0104_merged.cube')
        actual_filenames = ssiprun.create_cube_filenames_tri_surf(self.inchikey)
        self.assertEqual(expected_filenames, actual_filenames)
    def test_create_cube_filename_mono_surf(self):
        """Test expected name is returned.

        Returns
        -------
        None.

        """
        expected_filename = "XLYOFNOQVPJJNP-UHFFFAOYSA-N_0.0020_merged.cube"
        actual_filename = ssiprun.create_cube_filename_mono_surf(self.inchikey)
        self.assertEqual(expected_filename, actual_filename)
    def test_get_inchikey_stem(self):
        """Test expected stem is returned.

        Returns
        -------
        None.

        """
        expected_stem = "XLYOFNOQVPJJNP-UHFFFAOYSA-N"
        actual_stem = ssiprun.get_inchikey_stem("XLYOFNOQVPJJNP-UHFFFAOYSA-N.cml")
        self.assertEqual(expected_stem, actual_stem)