#    ssipfootprint provides automated batch SSIP footprinting functionality.
#    Copyright (C) 2020  Mark D. Driver
#
#    ssipfootprint is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
# -*- coding: utf-8 -*-
"""
Script for running tests.

:Authors:
    Mark Driver <mdd31>
"""

import logging
import unittest
import sys
from ssipfootprint.test.ssiprun_test import SSIPRunTestCase

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)

TESTCASES = [
    SSIPRunTestCase
]


def create_test_suite():
    """Create Test suite of all tests of ssip footprint.

    Returns
    -------
    suite : unittest.TestSuite
        Test suite containing all tests from .

    """
    LOGGER.info("setting up loader and test suite")
    loader = unittest.TestLoader()
    suite = unittest.TestSuite()
    for test_case in TESTCASES:
        LOGGER.debug("Adding %s", test_case)
        suite.addTests(loader.loadTestsFromTestCase(test_case))
    return suite


def run_tests():
    """Runs test suite. Exits if there is a failure.

    Returns
    --------
    None
    """
    LOGGER.info("calling test suite method")
    suite = create_test_suite()
    LOGGER.info("running test suite")
    ret = (
        not unittest.TextTestRunner(verbosity=2, stream=sys.stderr)
        .run(suite)
        .wasSuccessful()
    )
    if ret:
        sys.exit(ret)


if __name__ == "__main__":
    run_tests()

