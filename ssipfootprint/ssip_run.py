#    ssipfootprint provides automated batch SSIP footprinting functionality.
#    Copyright (C) 2020  Mark D. Driver
#
#    ssipfootprint is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
# -*- coding: utf-8 -*-
"""
Methods for running ssip calculation on multiple molecules.

:Authors:
    Mark Driver <mdd31>
"""

import logging
import glob
import subprocess

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)

LOGGER_INFO = logging.getLogger(__name__ + "output")
LOGGER.setLevel(logging.INFO)


def ssip_run(cml_directory, **kwargs):
    """Run SSIP calculations on all CML files within the input directory.

    Parameters
    ----------
    cml_directory : str
        Directory of interest.
    ssip_call : str
        SSIP program call- path to jar with dependencies. Default is ssip,
        if ssip is installed.
    num_surf : str
        number of surfaces to use: either mono (default) or tri.

    Returns
    -------
    ssip_return_call_values : list
        List of results of subprocess.call.

    """
    cml_filenames = get_cml_filenames(cml_directory)
    LOGGER_INFO.info("%i cml files found", len(cml_filenames))
    ssip_return_call_values = call_ssip_from_filenames(cml_filenames, **kwargs)
    LOGGER_INFO.info("return_values:")
    for ssip_return_call_value in ssip_return_call_values:
        LOGGER_INFO.info(ssip_return_call_value)
    return ssip_return_call_values


def get_cml_filenames(file_directory):
    """Get all CML files in a directory.
    
    Uses glob to find CML. Assumes all required cube files are located in the
    same folder and have the same basic file stem.

    Parameters
    ----------
    file_directory : str
        Directory of interest.

    Returns
    -------
    list
        CML filenames.

    """
    return glob.glob("{0}/*.cml".format(file_directory))


def call_ssip_from_filenames(cml_filename_list, **kwargs):
    """Call footprinting on each CML file in list.

    Parameters
    ----------
    cml_filename_list : list
        List of CML filenames.
    ssip_call : str
        SSIP program call- path to jar with dependencies. Default is ssip,
        if ssip is installed.
    num_surf : str
        number of surfaces to use: either mono (default) or tri.

    Returns
    -------
    ssip_call_return_values : list
        List of results of subprocess.call.

    """
    ssip_call_return_values = []
    for cml_filename in cml_filename_list:
        LOGGER.info("cml filename: %s", cml_filename)
        ssip_call_return_values.append(call_ssip_from_filename(cml_filename, **kwargs))
    return ssip_call_return_values


def call_ssip_from_filename(cml_filename, **kwargs):
    """Call SSIP footprinting on given CML file.

    Parameters
    ----------
    cml_filename : str
        CML filename.
    ssip_call : str
        SSIP program call- path to jar with dependencies. Default is ssip,
        if ssip is installed.
    num_surf : str
        number of surfaces to use: either mono (default) or tri.

    Returns
    -------
    TYPE
        result of subprocess.call.

    """
    ssip_call_list = None
    num_surf = kwargs.pop("num_surf", "mono")
    if num_surf == "mono":
        ssip_call_list = create_ssip_call_argument_list_mono_surf(
            cml_filename, **kwargs
        )
    elif num_surf == "tri":
        ssip_call_list = create_ssip_call_argument_list_tri_surf(cml_filename, **kwargs)
    LOGGER.info("argument call list:")
    LOGGER.info(ssip_call_list)
    return call_ssip_with_arguments(ssip_call_list)


def call_ssip_with_arguments(ssip_call_list):
    """Call footprinting on call list arguments.

    Parameters
    ----------
    ssip_call_list : list
        CLI arguments for running ssip jar.

    Returns
    -------
    TYPE
        result of subprocess.call.

    """
    return subprocess.call(ssip_call_list)


def create_ssip_call_argument_list_tri_surf(cml_filename, **kwargs):
    """Create SSIP program call arguments from CML filename for tri surface.

    Parameters
    ----------
    cml_filename : str
        CML filename.
    ssip_call : str
        SSIP program call- path to jar with dependencies. Default is ssip,
        if ssip is installed.

    Returns
    -------
    ssip_program_call : list
        Arguments for SSIP call.


    """
    inchikey = get_inchikey_stem(cml_filename)
    cubefile_names = create_cube_filenames_tri_surf(inchikey)
    ssip_program_call = create_ssip_call(**kwargs)
    ssip_program_call.extend(
        [
            "-t",
            "-m",
            cubefile_names[0],
            "-g",
            cubefile_names[1],
            "-f",
            cubefile_names[2],  # check options for surfaces.
            "-c",
            cml_filename,
            "-w",
            create_ssip_filename(inchikey),
        ]
    )
    return ssip_program_call


def create_ssip_call_argument_list_mono_surf(cml_filename, **kwargs):
    """Create SSIP program call arguments from CML filename for mono surface.

    Parameters
    ----------
    cml_filename : str
        CML filename.
    ssip_call : str
        SSIP program call- path to jar with dependencies. Default is ssip,
        if ssip is installed.

    Returns
    -------
    ssip_program_call : list
        Arguments for SSIP call.

    """
    inchikey = get_inchikey_stem(cml_filename)
    ssip_program_call = create_ssip_call(**kwargs)
    ssip_program_call.extend(
        [
            "-m",
            create_cube_filename_mono_surf(inchikey),
            "-c",
            cml_filename,
            "-w",
            create_ssip_filename(inchikey),
        ]
    )
    return ssip_program_call


def create_ssip_call(**kwargs):
    """Create list of call arguements foir SSIP program call part of arguments.

    Parameters
    ----------
    ssip_call : str
        SSIP program call- path to jar with dependencies. Default is ssip,
        if ssip is installed.

    Returns
    -------
    list
        arguments.

    """
    ssip_program_call = kwargs.pop("ssip_call", "ssip")
    if ssip_program_call != "ssip":
        return ["java", "-jar", "{0}".format(ssip_program_call)]
    else:
        return [ssip_program_call]


def create_ssip_filename(inchikey):
    """Generate SSIP XML filename from inchikey filestem.
    
    The file has the suffix '_ssip.xml', and will be located in the same
    directory as the input cml and cube files.

    Parameters
    ----------
    inchikey : str
        InChIKey filename stem for files.

    Returns
    -------
    str
        SSIP XML filename.

    """
    return inchikey + "_ssip.xml"


def create_cube_filenames_tri_surf(inchikey):
    """Create the filenames for the trisurface MEPS cubes based on InChIKey.

    Parameters
    ----------
    inchikey : str
        InChIKey filename stem for files.

    Returns
    -------
    str
        Filename for 0.0020 MEPS cube file.
    str
        Filename for 0.0050 MEPS cube file.
    str
        Filename for 0.0104 MEPS cube file.

    """
    return (
        inchikey + "_0.0020_merged.cube",
        inchikey + "_0.0050_merged.cube",
        inchikey + "_0.0104_merged.cube",
    )


def create_cube_filename_mono_surf(inchikey):
    """Create the filename for the 0.002 e Bohr^-3 MEPS cube based on InChIKey.

    Parameters
    ----------
    inchikey : str
        InChIKey filename stem for files.

    Returns
    -------
    str
        Filename for 0.0020 MEPS cube file.

    """
    return inchikey + "_0.0020_merged.cube"


def get_inchikey_stem(cml_filename):
    """Get inchikey stem from CML filename.
    
    Assumes files are named path/to/file/{inchikey}.cml so that the file stem
    can be extracted.

    Parameters
    ----------
    cml_filename : str
        CML filename.

    Returns
    -------
    str
        filename stem without .cml suffix.

    """
    return cml_filename.replace(".cml", "")
