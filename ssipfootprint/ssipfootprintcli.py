#    ssipfootprint provides automated batch SSIP footprinting functionality.
#    Copyright (C) 2020  Mark D. Driver
#
#    ssipfootprint is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
# -*- coding: utf-8 -*-
"""
Script for parsing command line input.

Contains main function that is run when called from the command line.

The CLI parser has two main modes of operation:

:Authors:
    Mark Driver <mdd31>

"""

import logging
import argparse
import ssipfootprint.ssip_run as ssip_run

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


def process_args(args):
    """Process CLI arguments.

    Parameters
    ----------
    args : argparse.ArgumentParser
        Argument parser.

    Returns
    -------
    list
        Results of footprinting calls.

    """
    return args.func(args)


def process_ssip_run_args(args):
    """Process CLI arguments- running SSIP footprinting based on input args.

    Parameters
    ----------
    args : argparse.ArgumentParser
        Argument parser.

    Returns
    -------
    list
        Results of footprinting calls.

    """
    CML_DIRECTORY = args.cml_directory
    kwargs = {"ssip_call": args.ssip_call}
    if args.mono:
        kwargs["num_surf"] = "mono"
    elif args.trisurf:
        kwargs["num_surf"] = "tri"

    return ssip_run.ssip_run(CML_DIRECTORY, **kwargs)


def create_argparser():
    """Create argument parser for batch calling of SSIP footrprinting.

    Returns
    -------
    argparse.ArgumentParser
        argument parser for batch SSIP footprinting.

    """
    # parser for running SSIP footprinting.
    ssip_run_description = """This can generate the SSIP XML files by calling the ssip program for each cml/cube file set
in the given directory. It requires cube files to follow the same naming convention as the cml file.

This provides the option to do it using the single MEPS description or triple
MEPS surface description (to be implemented here and in the JAVA code).
"""
    parser_ssip_run = argparse.ArgumentParser(
        description=ssip_run_description,
    )
    parser_ssip_run.add_argument(
        "cml_directory", type=str, help="Directory containing CML files and cube files."
    )
    mono_or_tri_ssip = parser_ssip_run.add_mutually_exclusive_group()
    mono_or_tri_ssip.add_argument(
        "-m",
        "--mono",
        "--monosurf",
        action="store_true",
        help="sets calculation to use single surface in calculation",
    )
    mono_or_tri_ssip.add_argument(
        "-t",
        "--trisurf",
        action="store_true",
        help="sets calculation to use three surfaces in calculation",
    )
    mono_or_tri_ssip.add_argument("--custom", action="store_true",
                                  help="Sets calculation to use a custom set of properties")
    parser_ssip_run.add_argument(
        "-s",
        "--ssip_call",
        type=str,
        default="ssip",
        help="call signature for SSIP program. (default: ssip, assumes deb is installed locally, if not a jar needs to be given)",
    )
    parser_ssip_run.set_defaults(func=process_ssip_run_args)
    return parser_ssip_run


def main():
    """Main method.
    
    Processes arguments from command line using the created argument parser, for
    the batch running of SSIP footprinting calculations.

    Returns
    -------
    list
        Results of ssip footprinting calls.

    """
    # Create parser
    parser = create_argparser()
    # parse args
    args = parser.parse_args()
    return process_args(args)
