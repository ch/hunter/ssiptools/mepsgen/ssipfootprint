# SSIPFootprint

Module for the batch running of SSIP footprinting calculations.

### Clone this repository ###

*From bitbucket:*

        git clone https://bitbucket.org/mdd31/ssipfootprint.git


*From University of Cambridge gitlab (using ssh):*

    to migrate

If you have not already set up ssh access to the gitlab server please look through the instructions on the gitlab help page [https://gitlab.developers.cam.ac.uk/help/ssh/README](https://gitlab.developers.cam.ac.uk/help/ssh/README).


### Python Environment set up ###

Change in to the repository:

        cd ssipfootprint

Details of an anaconda environment is provided in the repository with the required dependencies for this package (with python 3.7).

        conda env create -f environment.yml


#### Ziggy environment setup. ####

Note that if you are on ziggy you need to first load anaconda:

        module load anaconda/python3/4.4.0

Then install this environment instead (based on python 3.6):

        conda env create -f environment36.yml

This is due to NWChem compilation issues with anaconda v5.X

#### Python environment ####

This creates and environment callled 'nwchempy' which can be loaded using:

        conda activate nwchempy
        
The ziggy (3.6) environment is called nwchempy36.

### Install dependencies ###

Before installing this module please make sure that the required modules listed below are installed in the current python environment (resultsanalysis).

Before cloning the dependent repositories please change up a directory and follow install instructions to install within the readme files for the repository.
Do not follow the instructions to create a python environment.

#### Required modules ####

The other modules produced by Mark Driver required for this work to function are listed below. Please install them in the below order.

* xmlvalidator [bitbucket](https://bitbucket.org/mdd31/xmlvalidator) [CamGitlab](https://gitlab.developers.cam.ac.uk/ch/hunter/ssiptools/utils/xmlvalidator)
* ssip (specifically footprint module) [CamGitlab](https://gitlab.developers.cam.ac.uk/ch/hunter/ssip) - only required for batch generation of SSIP descriptions

For the SSIP phasetransfer module please compile a jar with dependencies as detailed in the repository documentation (This requires version 6.0.0 or greater).

### Installing SSIPFootprint ###

THis can be done using pip in the environment created earlier.

Build and install with pip using when in the top directory:

    pip install .

### Test CLI works ###

Test that the CLI works.
```
~$ python -m ssipfootprint -h
usage: __main__.py [-h] [-m | -t | --custom] [-s SSIP_CALL] cml_directory

This can generate the SSIP XML files by calling the ssip program for each
cml/cube file set in the given directory. It requires cube files to follow the
same naming convention as the cml file. This provides the option to do it
using the single MEPS description or triple MEPS surface description (to be
implemented here and in the JAVA code).

positional arguments:
  cml_directory         Directory containing CML files and cube files.

optional arguments:
  -h, --help            show this help message and exit
  -m, --mono, --monosurf
                        sets calculation to use single surface in calculation
  -t, --trisurf         sets calculation to use three surfaces in calculation
  --custom              Sets calculation to use a custom set of properties
  -s SSIP_CALL, --ssip_call SSIP_CALL
                        call signature for SSIP program. (default: ssip,
                        assumes deb is installed locally, if not a jar needs
                        to be given)
```

### Documentation ###

Documentation can be rendered using sphinx.

        cd docs
        make html

The rendered HTML can then be found in the build sub folder.

In progress: display of this documentation in a project wiki.

### Contribution guidelines ###

If you find any bugs please file an issue ticket.
Submission of pull requests for open issues or improvements are welcomed.

### Who do I talk to? ###

Any queries please contact Mark Driver.

### License

&copy; Mark Driver,  Christopher Hunter, Teodor Nikolov at the University of Cambridge

This is released under an AGPLv3 license for academic use.
Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:

 

Cambridge Enterprise Ltd
University of Cambridge
Hauser Forum
3 Charles Babbage Rd
Cambridge CB3 0GT
United Kingdom
Tel: +44 (0)1223 760339
Email: software@enterprise.cam.ac.uk